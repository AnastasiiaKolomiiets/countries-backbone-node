'use strict';

function Controller () {

    var list = new List(),
        views = {
            editView: new EditView(),
            propView: new PropView(),
            listView: new ListView({collection: list})
    },
    countryEditor = $('#countryEditor'),
    countryProperties = $('#countryProperties'),
    countryList =  $('#countryList');

    m.sub('country selected', informationShow);
    m.sub('country created', updateListView);
    m.sub('remove country', removeCountry);
    m.sub('index', start);
    m.sub('collection loaded', collectionLoaded);

    function start () {
        list.fetch({
            reset:true,
            success: (collection, response, options) => {
                m.pub('collection loaded');
            }
        });
        views.listView.render();
    }

    function collectionLoaded () {
        countryEditor.append(views.editView.render().el);
        countryList.append(views.listView.el);
        countryProperties.append(views.propView.render(list.at(0)).el);
    }

    function informationShow (model) {
        countryProperties.html('');
        if (list.length === 0) {
            start();
            m.sub('collection loaded', function () {
                if (typeof model === 'string') {
                    model = list.get(model);
                }
                countryProperties.append(views.propView.render(model).el);
            });
        } else {
            if (typeof model === 'string') {
                model = list.get(model);
            }
            countryProperties.append(views.propView.render(model).el);
        }
    }

    function updateListView (newCountry) {
        list.addNewCountry(newCountry);
    }

    function removeCountry (country) {
        if (list.length === 0) {
            start();
            m.sub('collection loaded', function () {
                country = list.get(country);
                country.destroy();
            });
        } else {
            if (typeof country === 'string'){
                country = list.get(country);
            }
            country.destroy();
        }
        countryList.append(views.listView.render().el);
    }

    return this;
};
