'use strict';

var Country =  Backbone.Model.extend({
	urlRoot: '/countries',
	defaults: {
		'name': 'name',
		'native': 'native',
		'capital': 'capital',
		'currency': 'currency'
	}
});
