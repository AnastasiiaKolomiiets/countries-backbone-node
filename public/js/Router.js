'use strict';

var Router = Backbone.Router.extend({
    routes: {
        "": 'start',
        'property/:id': 'showProperty',
        'remove/:id': 'removeCountryId',
    },

    start: function () {
        m.pub('index');
    },

    showProperty: function (id) {
        m.pub('country selected', id);
    },

    removeCountryId: function (id){
        m.pub('remove country', id);
    },

    initialize: function () {
		m.sub('index', this.index.bind(this));
		m.sub('country selected', this.countrySelected.bind(this));
		m.sub('remove country', this.removeCountry.bind(this));
	},

	index: function () {
		this.navigate('');
	},

	countrySelected: function (country) {
        if (typeof country !== 'string'){
            country = country.id;
        }
		this.navigate('property/' + country);
	},

	removeCountry: function (country) {
        if (typeof country !== 'string'){
            country = country.id;
        }
		this.navigate('remove/' + country);
	}
});
