'use strict';

var CountryView = Backbone.View.extend({
    tagName:'div',

    template: _.template('<p>\
                            <span><%= name %></span>\
                            <button>Remove</button>\
                         </p>'),

    events: {
        'click span': 'countrySelected',
        'click button':'removeCountry'
    },

    render: function () {
        var countryName = this.model.get('name')
        this.$el.html(this.template({name: countryName}));

		return this;
	},

    countrySelected: function () {
        console.dir(this.model);
        m.pub('country selected', this.model);
    },

    removeCountry: function () {
        m.pub('remove country', this.model);
    }
});
