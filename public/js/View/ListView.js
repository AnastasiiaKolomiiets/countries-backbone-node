'use strict';

var ListView =  Backbone.View.extend({
    tagName: 'div',

    events:{
        'remove': 'removeCountry'
    },

    initialize: function () {
        this.listenTo(this.collection, 'add', this.addOne);
        this.listenTo(this.collection, 'reset', this.render);
    },

    render: function () {
        this.$el.html('');
        this.addAll();

         return this;
    },

    addAll: function () {
        this.collection.forEach(this.addOne, this);
    },

    addOne: function (country) {
        var countryView = new CountryView({model: country});
        this.$el.append(countryView.render().el);
    }

});
