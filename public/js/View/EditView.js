'use strict';

var EditView = Backbone.View.extend({
    tagName: 'div',

    template: _.template('<p>Country:</p>\
                <div>\
                    <input type="text" name="name">\
                </div>\
                <p>Native:</p>\
                <div>\
                    <input type="text" name="native">\
                </div>\
                <p>Capital:</p>\
                <div>\
                    <input type="text" name="capital">\
                </div>\
                <p>Currency:</p>\
                <div>\
                    <input type="text" name="currency">\
                </div>\
                <p>\
    				<button>Create</button>\
    			</p>'),

    events:{
        'click button': 'countryСreated'
    },

    countryСreated: function () {
        var newCountryValue = $('input'),
            newCountry = {};
         newCountryValue.each(function (property) {
            newCountry[newCountryValue[property].name] = newCountryValue[property].value;
        });

        m.pub('country created', newCountry);
    },

    render: function () {
		this.$el.html(this.template);

		return this;
	}
});
