'use strict';

var PropView = Backbone.View.extend({
    tagName: 'div',
    template: _.template('<p>Country:</p>\
                <div name = "name">\
                    <%= name %>\
                </div>\
                <p>Native:</p>\
                <div>\
                    <%= native %>\
                </div>\
                <p>Capital:</p>\
                <div>\
                    <%= capital %>\
                </div>\
                <p>Currency:</p>\
                <div>\
                    <%= currency %>\
                </div>'),

    render: function (model) {
		this.$el.html(this.template(model.attributes));
        this.on('click button', function (model) {
            console.log('button remove works');
        });

		return this;
	}
});
