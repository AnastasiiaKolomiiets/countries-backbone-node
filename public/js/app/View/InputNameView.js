var InputNameView = Backbone.View.extend({
	tagName: 'div',

	events: {
		'click button': 'inputName',
	},

	render: function () {
		this.$el.html('<p><input type="text"></input></p><p><button>Click me!</button></p>');

		return this;
	},

	inputName: function () {
		var nameValue = $('input').val();
		router.navigate('' + nameValue, {trigger:true});
	}
});