var CatsView = Backbone.View.extend({
	tagName: 'div',

	events: {
		'click': 'showCats',
	},

	initialize: function () {
		this.collection = new CatCollection();
	},

	render: function () {
		this.$el.html('<button>Show Cats</button>');

		return this;
	},

	showCats: function () {
		this.$el.html('');

		this.collection.each(function (cat) {
			var cat = new SingleCatView({
				model: cat
			});

			this.$el.append(cat.render().el);
		}, this);
	}
});
