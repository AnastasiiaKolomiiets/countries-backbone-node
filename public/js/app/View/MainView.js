var MainView = Backbone.View.extend({
	tagName: 'div',

	events: {
		'click': 'inputName',
	},

	render: function () {
		this.$el.html('<h2>Try router</h2>');

		return this;
	},

	inputName: function () {
		router.navigate('inputName', {trigger:true});
	}
});