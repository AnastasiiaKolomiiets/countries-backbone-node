var ShowNameView = Backbone.View.extend({
	tagName: 'div',

	events: {
		'click button': 'inputName',
	},

	render: function (name) {
		this.$el.html('<p>' + name + '</p><button>Edit</button>');
		this.name = name;
		return this;
	},

	inputName: function () {
		router.navigate('inputName', {trigger:true});
	}
});