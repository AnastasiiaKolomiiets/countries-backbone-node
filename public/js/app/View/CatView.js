var SingleCatView = Backbone.View.extend({
	tagName: 'div',

	compiled: _.template('<%= name %> Likes: <%= likes %><button>Like</button>'),

	events: {
		'click button': 'addLike'
	},

	initialize: function () {
		this.model.on('change', this.render, this);
	},

	render: function () {
		this.$el.html(this.compiled(this.model.toJSON()));

		return this;
	},

	addLike: function () {
		// var counter = this.model.get('likes');
		// counter++;
		this.model.set({likes: this.model.get('likes') + 1});
		// this.render();
	}
});