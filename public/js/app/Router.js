var Router = Backbone.Router.extend({
	routes: {
		'': 'index',
		'inputName': 'inputName',
		':name': 'showName'
	},

	initialize: function () {
		this.container = $('#container');
	},

	index: function () {
		var mainView = new MainView();	
		this.container.html('');
		this.container.append(mainView.render().el);
	},

	inputName: function () {
		var inputNameView = new InputNameView();	
		this.container.html('');
		this.container.append(inputNameView.render().el);

	},

	showName: function (name) {
		var showNameView = new ShowNameView();	
		this.container.html('');
		this.container.append(showNameView.render(name).el);
	},
})

