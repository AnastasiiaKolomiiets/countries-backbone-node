var CatCollection = Backbone.Collection.extend({
	model: Cat,
	initialize: function () {
		this.add([{
			'name': 'Artur'
		},{
			'name': 'Dima'
		},{
			'name': 'Nastya'
		},{
			'name': 'Petya'
		},{
			'name': 'Alfa'
		},{
			'name': 'Kirill'
		}]);
	}
});