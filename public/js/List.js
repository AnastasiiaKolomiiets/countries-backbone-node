'use strict';

var List = Backbone.Collection.extend({
    url: '/countries',
    model: Country,
    parse: function (response) {
        return response.countries;
    },

    addNewCountry: function(newCountry) {
        this.create(newCountry);
    }
});
