window.addEventListener('load', main, false);

var m = new Mediator(),
	router;

function main () {
    var controller = new Controller;
    router = new Router();
    Backbone.history.start();
}
