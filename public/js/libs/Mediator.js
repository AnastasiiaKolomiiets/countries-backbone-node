'use strict';

function Mediator () {
    var listeners = [];


    this.sub = function (channel, callback) {
        console.log('mediator sub ' + channel);
        if (!listeners[channel]){
            listeners[channel] = [];
        }
        listeners[channel].push(callback);
    };

    this.pub = function (channel, value) {
        console.log('mediator pub ' + channel);
        if (listeners[channel]){
            listeners[channel].forEach(function (item) {
                item(value);
            });
        }
    };

    return this;
}
