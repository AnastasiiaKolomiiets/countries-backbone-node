function countries () {
    return [
            {"name": "Andorra", "native": "Andorra","capital": "Andorra la Vella", "currency": "EUR", "id":0},
            {"name": "Switzerland", "native": "Schweiz","capital": "Bern", "currency": "CHE,CHF,CHW", "id":1},
            {"name": "United Kingdom of Great Britain and Northern Ireland", "native": "English","capital": "London", "currency": "GBP", "id":2}
        ];
}

exports.countries = countries();
