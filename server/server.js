var http = require('http');
var fs = require('fs');
var path = require('path');
var mime = require('mime');
var countries = require('./countries');
var cache = {};

var server = http.createServer(function (request, response) {
    var filePath = false;
    var param = request.url.match(/countries/);

    if (request.url === '/index.html' || request.url === '/') {
        filePath = 'public/index.html';
        var absolutePath = '../' + filePath;
        serveStatic(response, cache, absolutePath);

    } else if (request.url === '/countries' && (request.method === 'GET')) {
        response.writeHead(200, {"Content-Type": "application/json"});
        response.write(JSON.stringify(countries));
        response.end();
        filePath = 'public/' + request.url;

    } else if (request.method === 'DELETE') {
        var countryId = request.url.split('/');
        countryId = countryId[countryId.length - 1];
        deleteCountry(countryId);
        response.write(JSON.stringify(countries));
        response.end();
        filePath = 'public/' + request.url;

    } else if (request.method === 'PUT') {
        var fullBody = '';
        request.on('data', function(chunk) {
            fullBody += chunk.toString();
        });

        request.on('end', function() {
            response.writeHead(200, {'Content-Type': 'text/html'});
            var createdCountry = JSON.parse(fullBody);
            //updateCountry(createdCountry);
            response.write(JSON.stringify(countries));
            response.end();
        });
    } else if (request.method === 'POST') {
        var fullBody = '';
        request.on('data', function(chunk) {
            fullBody += chunk.toString();
        });

        request.on('end', function() {
            response.writeHead(200, {'Content-Type': 'text/html'});
            var createdCountry = JSON.parse(fullBody);
            addNewCountry(createdCountry);
            response.write(JSON.stringify(createdCountry));
            response.end();
        });
    } else {
        filePath = 'public/' + request.url;
        var absolutePath = '../' + filePath;
        serveStatic(response, cache, absolutePath);
    }

});

function deleteCountry (countryId) {
    countries['countries'] = countries['countries'].filter(function (country) {
        console.log(country['id'].toString() === countryId);
        return country['id'].toString() !== countryId;
        }
    );
    console.dir('after delete: ' + countries.countries);
}

function addNewCountry (createdCountry) {
    createdCountry.id = countries.countries[countries.countries.length - 1].id + 1;
    countries.countries.push(createdCountry);
}

server.listen(3001, function () {
    console.log('server listening port 3001');
});

function sendFile (response, filePath, fileContents) {
    response.writeHead(200, {"Content-Type": mime.lookup(path.basename(filePath))});
    response.end(fileContents);
}

function sendError (response) {
    response.writeHead(404, {"Content-Type": "text/html"});
    response.end('not found');
}

function serveStatic (response, cache, absPath) {
    if (cache[absPath]) {
        sendFile(response, absPath, cache[absPath]);
    } else {
        fs.exists(absPath, function (exist) {
            if (exist) {
                fs.readFile(absPath, function (err, data) {
                    if (err) {
                        console.log('error');
                        sendError(response);
                    } else {
                        cache[absPath] = data;
                        sendFile(response, absPath, data)
                    }
                });
            } else {
                console.log(absPath,'do not exist');
                sendError(response);
            }
        });
    }
}
